package com.ykkblog.fastbase;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 响应对象
 *
 * @author YKK
 */
@Data
@ToString
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> {
    private Object status;
    private String message;
    private T data;
    private Integer pageNum;
    private Integer total;

    public static final transient Boolean SUCCESS = true;
    public static final transient Boolean ERROR = false;

    /**
     * <p>Constructor for Result.</p>
     *
     * @param status  a {@link java.lang.Object} object.
     * @param message a {@link java.lang.String} object.
     */
    public Result(Object status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * <p>Constructor for Result.</p>
     *
     * @param status  a {@link java.lang.Object} object.
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     */
    public Result(Object status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    /**
     * Status for Result.
     *
     * @return status
     */
    public Boolean ok() {
        return (Boolean) status;
    }
}
