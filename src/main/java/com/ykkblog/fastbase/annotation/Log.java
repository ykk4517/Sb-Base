package com.ykkblog.fastbase.annotation;


import com.ykkblog.fastbase.enums.BusinessType;
import com.ykkblog.fastbase.enums.OperatorType;

import java.lang.annotation.*;

/**
 * 自定义操作日志注解
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 模块
     */
    String module() default "";

    /**
     * 模块描述
     */
    String title() default "";

    /**
     * 功能
     */
    BusinessType businessType() default BusinessType.OTHER;

    /**
     * 操作人类别
     */
    OperatorType operatorType() default OperatorType.MANAGE;

    /**
     * 是否保存请求的参数
     */
    boolean isSaveRequestData() default true;

    /**
     * 是否保存响应的数据
     */
    boolean isSaveResponseData() default true;

}
