package com.ykkblog.fastbase.annotation;

import com.ykkblog.fastbase.enums.Logical;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限管理
 *
 * @author YKK
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequirePermission {
    /**
     * 需要的权限数组
     */
    String[] value();

    /**
     * 权限连接条件
     */
    Logical logical() default Logical.AND;
}
