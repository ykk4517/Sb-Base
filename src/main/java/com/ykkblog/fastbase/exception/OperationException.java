package com.ykkblog.fastbase.exception;

/**
 * 操作异常
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
public class OperationException extends RuntimeException {

    /**
     * 异常码
     */
    protected int code;

    public OperationException() {
    }

    public OperationException(Throwable cause) {
        super(cause);
    }

    public OperationException(String message) {
        super(message);
    }

    public OperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public OperationException(int code, String message) {
        super(message);
        this.code = code;
    }

    public OperationException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
