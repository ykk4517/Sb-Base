package com.ykkblog.fastbase.exception;

/**
 * 分页异常
 *
 * @author YKK
 */
public class PageException extends RuntimeException {

    /**
     * 异常码
     */
    protected int code;

    public PageException() {
    }

    public PageException(Throwable cause) {
        super(cause);
    }

    public PageException(String message) {
        super(message);
    }

    public PageException(String message, Throwable cause) {
        super(message, cause);
    }

    public PageException(int code, String message) {
        super(message);
        this.code = code;
    }

    public PageException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
