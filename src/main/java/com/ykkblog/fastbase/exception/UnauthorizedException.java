package com.ykkblog.fastbase.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 未授权异常
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UnauthorizedException extends RuntimeException {

    /**
     * 异常信息
     */
    public String message;

    /**
     * Constructor for UnauthorizedException.
     *
     * @param message 提示信息
     */
    public UnauthorizedException(String message) {
        super(message);
        this.message = message;
    }

}
