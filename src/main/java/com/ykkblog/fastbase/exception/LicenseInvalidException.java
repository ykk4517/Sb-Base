package com.ykkblog.fastbase.exception;

import java.util.Base64;
import java.util.Random;

/**
 * 许可证无效异常
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
public class LicenseInvalidException extends RuntimeException {
    /**
     * 随机器
     */
    private static final Random RANDOM = new Random();

    /**
     * 错误信息（已编码为Base64字符串）
     */
    private static final String[] MESSAGES = {
            // 警告：无效的数据库连接。
            "6K2m5ZGK77ya5peg5pWI55qE5pWw5o2u5bqT6L+e5o6l44CC",
            // Warning: invalid database connection.
            "V2FybmluZzogaW52YWxpZCBkYXRhYmFzZSBjb25uZWN0aW9uLg==",
            // 警告：正在尝试连接数据库，请稍等。
            "6K2m5ZGK77ya5q2j5Zyo5bCd6K+V6L+e5o6l5pWw5o2u5bqT77yM6K+356iN562J",
            // Warning: trying to connect to database, please wait.
            "V2FybmluZzogdHJ5aW5nIHRvIGNvbm5lY3QgdG8gZGF0YWJhc2UsIHBsZWFzZSB3YWl0Lg==",
            // 警告：正在进行数据备份或还原，请稍等。
            "6K2m5ZGK77ya5q2j5Zyo6L+b6KGM5pWw5o2u5aSH5Lu95oiW6L+Y5Y6f77yM6K+356iN562J44CC",
            // Warning: data backup or restore in progress, please wait.
            "V2FybmluZzogZGF0YSBiYWNrdXAgb3IgcmVzdG9yZSBpbiBwcm9ncmVzcywgcGxlYXNlIHdhaXQu",
            // 警告：关键服务未就绪。
            "6K2m5ZGK77ya5YWz6ZSu5pyN5Yqh5pyq5bCx57uq44CC",
            // Warning: critical service is not ready.
            "V2FybmluZzogY3JpdGljYWwgc2VydmljZSBpcyBub3QgcmVhZHku",
            // 警告：关键服务正在启动，请稍等。
            "6K2m5ZGK77ya5YWz6ZSu5pyN5Yqh5q2j5Zyo5ZCv5Yqo77yM6K+356iN562J44CC",
            // Warning: critical service is starting, please wait.
            "V2FybmluZzogY3JpdGljYWwgc2VydmljZSBpcyBzdGFydGluZywgcGxlYXNlIHdhaXQu",
            // 警告：资源文件丢失。
            "6K2m5ZGK77ya6LWE5rqQ5paH5Lu25Lii5aSx44CC",
            // Warning: the resource file is missing.
            "V2FybmluZzogdGhlIHJlc291cmNlIGZpbGUgaXMgbWlzc2luZy4=",
            // 警告：正在加载资源文件，请稍等。
            "6K2m5ZGK77ya5q2j5Zyo5Yqg6L296LWE5rqQ5paH5Lu277yM6K+356iN562J44CC",
            // Warning: Please wait while the resource file is being loaded.
            "V2FybmluZzogUGxlYXNlIHdhaXQgd2hpbGUgdGhlIHJlc291cmNlIGZpbGUgaXMgYmVpbmcgbG9hZGVkLg==",
            // 警告：创建临时文件失败。
            "6K2m5ZGK77ya5Yib5bu65Li05pe25paH5Lu25aSx6LSl44CC",
            // Warning: Failed to create temporary file.
            "V2FybmluZzogRmFpbGVkIHRvIGNyZWF0ZSB0ZW1wb3JhcnkgZmlsZS4=",
            // 警告：内部网络忙，尝试连接失败。
            "6K2m5ZGK77ya5YaF6YOo572R57uc5b+Z77yM5bCd6K+V6L+e5o6l5aSx6LSl44CC",
            // Warning: The internal network is busy and the attempt to connect failed.
            "V2FybmluZzogVGhlIGludGVybmFsIG5ldHdvcmsgaXMgYnVzeSBhbmQgdGhlIGF0dGVtcHQgdG8gY29ubmVjdCBmYWlsZWQu",
            // 警告：依赖包缺失或不可用。
            "6K2m5ZGK77ya5L6d6LWW5YyF57y65aSx5oiW5LiN5Y+v55So44CC",
            // Warning: Dependency packages are missing or unavailable.
            "V2FybmluZzogRGVwZW5kZW5jeSBwYWNrYWdlcyBhcmUgbWlzc2luZyBvciB1bmF2YWlsYWJsZS4=",
            // 警告：可用内存不足。
            "6K2m5ZGK77ya5Y+v55So5YaF5a2Y5LiN6Laz44CC",
            // Warning: Not enough memory is available.
            "V2FybmluZzogTm90IGVub3VnaCBtZW1vcnkgaXMgYXZhaWxhYmxlLg==",
            // 警告：环境变量缺失。
            "6K2m5ZGK77ya546v5aKD5Y+Y6YeP57y65aSx44CC",
            // Warning: The environment variable is missing.
            "V2FybmluZzogVGhlIGVudmlyb25tZW50IHZhcmlhYmxlIGlzIG1pc3Npbmcu",
    };

    private static long lastTime = 0L;
    private static int lastIndex = 0;
    private static final long MAX_CACHE_TIME = 10000;
    private static final Object LOCK = new Object();

    public LicenseInvalidException() {
        super();
    }

    public LicenseInvalidException(String message) {
        super(message);
    }

    public LicenseInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public LicenseInvalidException(Throwable cause) {
        super(cause);
    }

    protected LicenseInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getMessage() {
        long currTime = System.currentTimeMillis();
        if (lastTime + MAX_CACHE_TIME < currTime) {
            synchronized (LOCK) {
                if (lastTime + MAX_CACHE_TIME < currTime) {
                    lastIndex = RANDOM.nextInt(MESSAGES.length);
                    lastTime = currTime;
                }
            }
        }

        String message = MESSAGES[lastIndex];
        return new String(Base64.getDecoder().decode(message));
    }
}
