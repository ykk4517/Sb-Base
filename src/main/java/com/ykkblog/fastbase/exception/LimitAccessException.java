package com.ykkblog.fastbase.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 请求过于频繁
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LimitAccessException extends RuntimeException {

    /**
     * 异常信息
     */
    public String message;

    /**
     * Constructor for LimitAccessException.
     *
     * @param message 提示信息
     */
    public LimitAccessException(String message) {
        super(message);
        this.message = message;
    }

}
