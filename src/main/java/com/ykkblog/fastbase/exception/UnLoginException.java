package com.ykkblog.fastbase.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 未登录或过期
 *
 * @author YKK
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UnLoginException extends RuntimeException {

    /**
     * 异常信息
     */
    public String message;

    /**
     * Constructor for UnLoginException.
     *
     * @param message 提示信息
     */
    public UnLoginException(String message) {
        super(message);
        this.message = message;
    }

}
