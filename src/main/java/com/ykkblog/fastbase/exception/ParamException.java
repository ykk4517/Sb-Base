package com.ykkblog.fastbase.exception;

/**
 * 入参异常
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
public class ParamException extends RuntimeException {

    /**
     * 异常码
     */
    protected int code;

    public ParamException() {
    }

    public ParamException(Throwable cause) {
        super(cause);
    }

    public ParamException(String message) {
        super(message);
    }

    public ParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParamException(int code, String message) {
        super(message);
        this.code = code;
    }

    public ParamException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
