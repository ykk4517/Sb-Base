package com.ykkblog.fastbase.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 资源不存在
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NotFoundException extends RuntimeException {

    /**
     * 异常信息
     */
    public String message;

    /**
     * Constructor for NotFoundException.
     *
     * @param message 提示信息
     */
    public NotFoundException(String message) {
        super(message);
        this.message = message;
    }

}
