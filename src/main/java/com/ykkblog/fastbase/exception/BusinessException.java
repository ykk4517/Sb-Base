package com.ykkblog.fastbase.exception;

/**
 * 业务异常
 *
 * @author 姚康康
 * @date 2021/9/9 20:31
 */
public class BusinessException extends RuntimeException {

    /**
     * 异常码
     */
    protected int code;

    public BusinessException() {
        super("操作失败，请稍后重试！");
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(int code, String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(int code, String msgFormat, Object... args) {
        super(String.format(msgFormat, args));
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
