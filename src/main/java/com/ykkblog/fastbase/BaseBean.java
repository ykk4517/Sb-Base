package com.ykkblog.fastbase;

import lombok.Data;

import javax.validation.constraints.Min;

/**
 * 入参基础类，分页参数
 *
 * @author YKK
 */
@Data
public class BaseBean {
    /**
     * 当前页数，默认为1
     */
    @Min(value = 1, message = "当前页数不合法")
    private Integer num = 1;
    /**
     * 每页条数，默认为10
     */
    @Min(value = 10, message = "条数不正确")
    private Integer size = 10;
    /**
     * 当前用户ID
     */
    private Integer currentUserId;
    /**
     * 搜索关键词
     */
    private String keywords;
}
