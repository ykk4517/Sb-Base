package com.ykkblog.fastbase.enums;

/**
 * 关系类型枚举
 *
 * @author 姚康康
 * @date 2021/9/9 20:07
 */
public enum Logical {
    /**
     * 与
     */
    AND,
    /**
     * 或
     */
    OR;

    private Logical() {
    }
}