package com.ykkblog.fastbase.enums;

/**
 * 日志类型
 *
 * @author 姚康康
 * @date 2021/9/9 20:31
 */
public enum BusinessType {

    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT,

    /**
     * 强退
     */
    FORCE,

    /**
     * 生成代码
     */
    GENCODE,

    /**
     * 清空数据
     */
    CLEAN,

    /**
     * 状态
     */
    STATUS,

    /**
     * 批量删除
     */
    BATCH_DELETE,

}
