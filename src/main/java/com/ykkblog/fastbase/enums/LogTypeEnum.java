package com.ykkblog.fastbase.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日志类型枚举
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
public enum LogTypeEnum {
    /**
     * 操作日志
     */
    OPERATION_LOG("10", "操作日志"),
    /**
     * 登录日志
     */
    LOGIN_LOG("20", "登录日志"),
    /**
     * 退出登录日志
     */
    LOGINOUT_LOG("00", "退出登录日志"),
    /**
     * 新增操作
     */
    INSERT_LOG("30", "新增操作"),
    /**
     * 修改操作
     */
    UPDATE_LOG("40", "修改操作"),
    /**
     * 删除操作
     */
    DELETE_LOG("50", "删除操作"),
    /**
     * 异常日志
     */
    EXCEPTION_LOG("60", "异常日志");

    /**
     * The Type.
     */
    String type;
    /**
     * The Name.
     */
    String name;

    LogTypeEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets name.
     *
     * @param type the type
     * @return the name
     */
    public static String getName(String type) {
        for (LogTypeEnum ele : LogTypeEnum.values()) {
            if (type.equals(ele.getType())) {
                return ele.getName();
            }
        }
        return null;
    }

    /**
     * Gets enum.
     *
     * @param type the type
     * @return the enum
     */
    public static LogTypeEnum getEnum(String type) {
        for (LogTypeEnum ele : LogTypeEnum.values()) {
            if (type.equals(ele.getType())) {
                return ele;
            }
        }
        return LogTypeEnum.OPERATION_LOG;
    }

    /**
     * Gets list.
     *
     * @return the list
     */
    public static List<Map<String, Object>> getList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (LogTypeEnum ele : LogTypeEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("key", ele.getType());
            map.put("value", ele.getName());
            list.add(map);
        }
        return list;
    }
}
