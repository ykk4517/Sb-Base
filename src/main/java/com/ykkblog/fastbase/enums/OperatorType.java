package com.ykkblog.fastbase.enums;

/**
 * 操作类型枚举
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
public enum OperatorType {

    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE

}
