package com.ykkblog.fastbase.enums;

/**
 * 数据类型枚举
 *
 * @author 姚康康
 * @date 2021/9/9 20:11
 */
public enum DataType {
    /**
     * 文本
     */
    Text,
    /**
     * 金额
     */
    Money,
    /**
     * 分数
     */
    Point,
    /**
     * 数字
     */
    Number,
    /**
     * 浮点数
     */
    Float,
    /**
     * 手机号码
     */
    Phone,
    /**
     * email
     */
    Email,
    /**
     * 身份证
     */
    IdCard,
    /**
     * 布尔
     */
    Boolean,
    /**
     * 字符
     */
    Char
}
