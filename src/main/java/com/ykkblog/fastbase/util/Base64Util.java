package com.ykkblog.fastbase.util;

import java.util.Base64;

/**
 * Base64Util加密工具类
 *
 * @author 姚康康
 * @date 2021/9/9 20:30
 */
public class Base64Util {

    /**
     * 加密
     *
     * @param bytes bytes
     * @return String
     */
    public static String baseEncode(byte[] bytes) {
        // Base64 加密
        return Base64.getEncoder().encodeToString(bytes);
        // return (new BASE64Encoder()).encodeBuffer(bytes);
    }

    /**
     * 加密
     *
     * @param str str
     * @return String
     */
    public static String baseEncode(String str) {
        byte[] bytes = str.getBytes();
        // Base64 加密
        return Base64.getEncoder().encodeToString(bytes);
        // return (new BASE64Encoder()).encodeBuffer(bytes);
    }

    /**
     * 解密
     * byte[] decoded = Base64.getMimeDecoder().decode(str);
     *
     * @param str str
     * @return String
     */
    public static String baseDecode(String str) {
        // Base64 解密
        try {
            byte[] decoded = Base64.getDecoder().decode(str);
            return new String(decoded);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * BASE64解密
     *
     * @param key key
     * @return String
     */
    public static String decryptBase64(String key) {
        return new String((Base64.getDecoder().decode(key)));
    }

    public static void main(String[] args) {
        String str = "设备禁止访问！";
        String encode = baseEncode(str);
        System.out.println(encode);
        System.out.println(baseDecode(encode + "奥术大师大奥"));
    }

}
