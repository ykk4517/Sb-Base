package com.ykkblog.fastbase.util;

/**
 * @author YKK
 * @date 2021/9/9 20:10
 **/

public class ConstantBase {
    public static final String LOCALHOST = "127.0.0.1";
    public static final String TAOBAO_HOST = "http://ip.taobao.com/service/getIpInfo.php";
    public static final String IP138_HOST = "https://www.ip138.com/iplookup.asp";
    /**
     * form-data or get ip: 180.136.216.131 accessKey: alibaba-inc
     * code: 0
     * data: {area: "", country: "中国", isp_id: "100017", queryIp: "180.136.216.131", city: "南宁",…}
     * msg: "query success"
     */
    public static final String TAOBAO_HOST1 = "https://ip.taobao.com/outGetIpInfo";

    /**
     * , 逗号
     */
    public static final String COMMA = ",";
    /**
     * . 点 英文句号
     */
    public static final String DOT = "\\.";
    /**
     * ; 分号
     */
    public static final String SEMICOLON = ";";
    /**
     * : 冒号
     */
    public static final String COLON = ":";
    /**
     * | 竖线
     */
    public static final String VERTICAL_BAR = "\\|";
    /**
     * _ 下划线
     */
    public static final String UNDERLINE = "_";
    /**
     * POST
     */
    public static final String POST_STR = "POST";
    /**
     * GET_STR
     */
    public static final String GET_STR = "GET";
    /**
     * NULL_STR
     */
    public static final String NULL_STR = "null";

    public static final String ID = "id";
    /**
     * token
     */
    public static final String TOKEN = "token";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String CAPTCHA = "captcha";
    public static final String OPTIONS = "OPTIONS";
    public static final String APPLICATION_JSON = "application/json";
    public static final String XML_HTTP_REQUEST = "XMLHttpRequest";
    public static final String TRUE = "1";
    public static final String FALSE = "0";

    public static final String XLS = "xls";
    public static final String XLS2 = ".xls";
    public static final String XLSX = "xlsx";
    public static final String XLSX2 = ".xlsx";
    public static final String PDF = "pdf";
    public static final String PDF2 = ".pdf";

    /**
     * 查找空格
     * \n 回车(\u000a)
     * \t 水平制表符(\u0009)
     * \s 空格(\u0008)
     * \r 换行(\u000d)
     */
    // public static final String REGEX_BLANK = "[\\s\\n\\t　]";
    // public static final String REGEX_BLANK = "\\s*|\r|\n|\t|　";
    public static final String REGEX_BLANK = "\\s|\r|\n|\t|　";

    /**
     * 基础状态对照码
     */
    public interface BasicNumber {
        /**
         * 零
         */
        int ZERO = 0;
        /**
         * 一
         */
        int ONE = 1;
        /**
         * 二
         */
        int TWO = 2;
        /**
         * 三
         */
        int THREE = 3;
        /**
         * 四
         */
        int FOUR = 4;
        /**
         * 五
         */
        int FIVE = 5;
        /**
         * 六
         */
        int SIX = 6;
        /**
         * 七
         */
        int SEVEN = 7;
        /**
         * 八
         */
        int EIGHT = 8;
        /**
         * 九
         */
        int NINE = 9;
    }

    /**
     * 字符串数字
     */
    public final static String[] NUM_STR = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public static void main(String[] args) {
        String str = "\t\t\t45122　32006012\r\r\n30925";
        System.out.println("原始：【" + str + "】");
        str = str.replaceAll(REGEX_BLANK, "@");
        System.out.println("结果：【" + str + "】");
        str = "62　89　XXXXXX X   XXXX \n\n\n";
        System.out.println("原始：【" + str + "】");
        str = str.replaceAll(REGEX_BLANK, "@");
        System.out.println("结果：【" + str + "】");
    }
}
