package com.ykkblog.fastbase.util;

import com.ykkblog.fastbase.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.*;


/**
 * map相关工具类
 *
 * @author YKK
 * @date 2021/9/10 14:19
 **/
@Slf4j
public class Map2List {

    /**
     * list map 转map
     *
     * @param listMap    listMap
     * @param columnName 主键列的key
     * @return map
     */
    public static Map<String, Map<String, Object>> parseListMapData(List<Map<String, Object>> listMap, String columnName) {

        Map<String, Map<String, Object>> stringListMap = new HashMap<>(listMap.size());
        for (Map<String, Object> stringObjectMap : listMap) {
            stringListMap.put(getStringValue(stringObjectMap.get(columnName)), stringObjectMap);
        }
        return stringListMap;
    }

    /**
     * map转list
     *
     * @param listMap    listMap
     * @param columnName 主键列的key
     * @return map
     *//*
    public static Map<String, List<String>> parseMapData(List<Map<String, Object>> listMap, String columnName) {

        Map<String, List<String>> stringListMap = new HashMap<>(listMap.size());
        for (Map<String, Object> stringObjectMap : listMap) {
            String[] split = columnName.split(DOT);
            stringListMap.put(getStringValue(stringObjectMap.get(split[split.length - 1])),
                    map2List(stringObjectMap));
        }
        return stringListMap;
    }*/

    /**
     * map转list
     *
     * @param listMap listMap
     */
    public static List<List<Object>> listMap2listList(List<Map<String, Object>> listMap) {

        List<List<Object>> listArrayList = new ArrayList<>(listMap.size());
        for (Map<String, Object> stringObjectMap : listMap) {
            listArrayList.add(map2List(stringObjectMap));
        }
        return listArrayList;
    }

    /**
     * map转list
     *
     * @param map map
     */
    public static List<Object> map2List(Map<String, Object> map) {

        List<Object> objectList = new ArrayList<>(map.size());
        for (String s : map.keySet()) {
            Object obj = map.get(s);
            /*if (obj == null){
                obj = "";
            }*/
            objectList.add(obj);
        }
        return objectList;
    }

    /**
     * map转list
     *
     * @param map map
     */
    public static List<String> map2ListString(Map<String, Object> map) {

        List<String> objectList = new ArrayList<>(map.size());
        for (String s : map.keySet()) {
            Object obj = map.get(s);
            objectList.add(getStringValue(obj));
        }

        return objectList;
    }

    /**
     * 获取字符串类型的值
     *
     * @param obj 数据对象
     * @return 字符串值
     */
    private static String getStringValue(Object obj) {
        if (obj == null || "".equals(obj)) {
            return "";
        }
        if (obj instanceof String || obj instanceof BigDecimal) {
            return obj.toString();
        } else if (obj instanceof Integer || obj instanceof Double || obj instanceof Boolean || obj instanceof Character) {
            return String.valueOf(obj);
        } else if (obj instanceof Date) {
            return obj.toString().replace(".0", "");
        }
        log.error("未知的数据类型：" + obj);
        throw new BusinessException("未知的数据类型，请联系管理员！");
    }

}
