package com.ykkblog.fastbase.util;

import com.ykkblog.fastbase.BaseItem;
import com.ykkblog.fastbase.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Slf4j
public class DateUtil {

    public static final String DEFAULT_PATTERNS = "yyyy-MM-dd HH:mm:ss";

    public static String getTimestamp() {
        return String.valueOf(System.currentTimeMillis() / 1000L);
    }

    /**
     * 字符串日期转时间戳
     *
     * @param dateStr  日期字符串
     * @param patterns 格式
     * @return 返回日期时间戳
     * @throws ParseException 转换异常
     */
    public static Long getTimestamp(String dateStr, String patterns) throws ParseException {
        // yyyy-MM-dd HH:mm:ss
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patterns);
        Date date = simpleDateFormat.parse(dateStr);
        return date.getTime();
    }

    /**
     * 获取年月日String类型的日期
     */
    public static String getStringDate() {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return getStringDate(DEFAULT_PATTERNS);
    }

    /**
     * 获取指定patterns格式的日期 yyyy-MM-dd/HH:mm:ss
     *
     * @param patterns 格式
     * @return 字符串日期
     */
    public static String getStringDate(@Nullable String patterns) {
        if (patterns == null || DataUtil.isEmpty(patterns)) {
            patterns = "yyyyMMddHHmmss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(patterns);
        return sdf.format(new Date());
    }

    /**
     * 获取指定patterns格式的日期 yyyy-MM-dd/HH:mm:ss
     *
     * @param dateStr  日期字符串
     * @param patterns 格式
     * @return 字符串日期
     * @throws ParseException 转换异常
     */
    public static String getStringDate(String dateStr, String patterns) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(patterns);
        sdf.setLenient(false);
        return sdf.format(sdf.parse(dateStr));
    }

    /**
     * 调整日期 yyyy-MM-dd HH:mm:ss
     * canlandar.add(Calendar.SECOND,10 );
     *
     * @param baseItems 调整参数
     * @return 字符串日期
     * @throws ParseException 转换异常
     */
    public static String getStringDate(BaseItem... baseItems) throws ParseException {
        return getStringDate(getStringDate(DEFAULT_PATTERNS), DEFAULT_PATTERNS, baseItems);
    }

    /**
     * 调整日期 yyyy-MM-dd HH:mm:ss
     * canlandar.add(Calendar.SECOND,10 );
     *
     * @param patterns  格式
     * @param baseItems 调整参数
     * @return 字符串日期
     * @throws ParseException 转换异常
     */
    public static String getStringDate(String patterns, BaseItem... baseItems) throws ParseException {
        return getStringDate(getStringDate(patterns), patterns, baseItems);
    }

    /**
     * 调整日期 yyyy-MM-dd HH:mm:ss
     * canlandar.add(Calendar.SECOND,10 );
     *
     * @param dateStr   日期字符串
     * @param patterns  格式
     * @param baseItems 调整参数
     * @return 字符串日期
     * @throws ParseException 转换异常
     */
    public static String getStringDate(String dateStr, String patterns, BaseItem... baseItems) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat(patterns);
        Date date = df.parse(dateStr);
        Calendar canlandar = Calendar.getInstance();
        canlandar.setTime(date);
        for (BaseItem item : baseItems) {
            canlandar.add(item.idInteger(), item.codeInteger());
        }
        return df.format(canlandar.getTime());
    }

    /**
     * 将时间戳转换为时间
     *
     * @param stamp stamp
     * @return String
     */
    public static String stampToDate(String stamp) {
        if (StringUtils.hasText(stamp)) {
            return "----";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(new Long(stamp)));
    }

    /**
     * 获取上一个月1号0点0分0秒的时间戳
     **/
    public static Long getPreviousMonthBegin() {
        Calendar cal = Calendar.getInstance();
        //获取上个月的日期（上个月的1号0点0分0秒）
        cal.setTime(new Date());
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 1, 1, 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis() / 1000;
    }

    /**
     * 当前时间是否在范围内
     *
     * @param start 开始时间
     * @param endT  截止时间
     * @return 当前时间是否在范围内
     */
    public static Boolean belongCalendar(String start, String endT) {
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date nowTime;
        Date beginTime;
        Date endTime;
        try {
            nowTime = df.parse(df.format(new Date()));
            beginTime = df.parse(start);
            endTime = df.parse(endT);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("日期格式有误！");
            throw new BusinessException("日期格式有误！");
        }
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        // 配置时段类似于  beginTime：07：00   endTime：00：30  时，结束时段天数加一天  Calendar.get(Calendar.HOUR_OF_DAY)
        //if (endTime.getHours() < beginTime.getHours()) {
        if (end.get(Calendar.HOUR_OF_DAY) < begin.get(Calendar.HOUR_OF_DAY)) {
            end.add(Calendar.DATE, 1);
        } else if (end.get(Calendar.HOUR_OF_DAY) == begin.get(Calendar.HOUR_OF_DAY)) {
            if (end.get(Calendar.MINUTE) < begin.get(Calendar.MINUTE)) {
                end.add(Calendar.DATE, 1);
            }
        }
        return date.after(begin) && date.before(end);
    }


    public static void main(String[] args) throws ParseException {
        String date = DateUtil.getStringDate("2021.08.31", "yyyy.MM.dd");
        System.out.println(date);

        System.out.println(getStringDate("2021.12.5", "yyyy.MM.dd", new BaseItem(Calendar.YEAR, 3)));

    }


}
