package com.ykkblog.fastbase.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.ykkblog.fastbase.util.DataUtil.isEmpty;

/**
 * 身份证工具类
 *
 * @author 姚康康
 * @date 2021/9/8 23:54
 */
public class IdCardUtil {

    final static Map<Integer, String> ZONENUM = new HashMap<>();

    public final static String REGEX = "\\d{17}(\\d|x|X)$";

    static {
        ZONENUM.put(11, "北京");
        ZONENUM.put(12, "天津");
        ZONENUM.put(13, "河北");
        ZONENUM.put(14, "山西");
        ZONENUM.put(15, "内蒙古");
        ZONENUM.put(21, "辽宁");
        ZONENUM.put(22, "吉林");
        ZONENUM.put(23, "黑龙江");
        ZONENUM.put(31, "上海");
        ZONENUM.put(32, "江苏");
        ZONENUM.put(33, "浙江");
        ZONENUM.put(34, "安徽");
        ZONENUM.put(35, "福建");
        ZONENUM.put(36, "江西");
        ZONENUM.put(37, "山东");
        ZONENUM.put(41, "河南");
        ZONENUM.put(42, "湖北");
        ZONENUM.put(43, "湖南");
        ZONENUM.put(44, "广东");
        ZONENUM.put(45, "广西");
        ZONENUM.put(46, "海南");
        ZONENUM.put(50, "重庆");
        ZONENUM.put(51, "四川");
        ZONENUM.put(52, "贵州");
        ZONENUM.put(53, "云南");
        ZONENUM.put(54, "西藏");
        ZONENUM.put(61, "陕西");
        ZONENUM.put(62, "甘肃");
        ZONENUM.put(63, "青海");
        ZONENUM.put(64, "宁夏");
        ZONENUM.put(65, "新疆");
        ZONENUM.put(71, "台湾");
        ZONENUM.put(81, "香港");
        ZONENUM.put(82, "澳门");
        ZONENUM.put(91, "外国");
    }

    /**
     * 身份证号校验 （支持18位）
     */
    public static Boolean checkCardNo(String cardNo) {
        boolean bb = cardNo.length() != 15 && cardNo.length() != 18;
        if (isEmpty(cardNo) || bb) {
            return false;
        }

        if (!cardNo.matches(REGEX)) {
            return false;
        }

        int index = 2;
        // 校验区位码
        if (!ZONENUM.containsKey(Integer.valueOf(cardNo.substring(0, index)))) {
            return false;
        }

        boolean isOldCard = cardNo.length() == 15;

        // 校验年份
        // 7-10位是出生年份，范围应该在1900-当前年份之间
        String idcardCalendar = getIdcardCalendar(cardNo);
        if (idcardCalendar == null) {
            return false;
        }
        int year = isOldCard ? Integer.parseInt(idcardCalendar) : Integer.parseInt(cardNo.substring(6, 10));
        int yearFirst = 1900;
        if (year < yearFirst || year > Calendar.getInstance().get(Calendar.YEAR)) {
            // 1900年的PASS，超过今年的PASS
            return false;
        }

        int monthMax = 12;

        // 11-12位代表出生月份，范围应该在01-12之间
        int month = isOldCard ? Integer.parseInt(cardNo.substring(8, 10)) : Integer.parseInt(cardNo.substring(10, monthMax));
        if (month < 1 || month > monthMax) {
            return false;
        }

        int day = isOldCard ? Integer.parseInt(cardNo.substring(10, monthMax)) : Integer.parseInt(cardNo.substring(monthMax, 14));
        // 13-14位是出生日期，范围应该在01-31之间
        int dayMax = 31;
        if (day < 1 || day > dayMax) {
            return false;
        }

        if (isOldCard) {
            return true;
        }

        // 校验第18位
        // S = Sum(Ai * Wi), i = 0, ... , 16 ，先对前17位数字的权求和
        // Ai:表示第i位置上的身份证号码数字值
        // Wi:表示第i位置上的加权因子
        // Wi: 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2


        // 18位身份证中最后一位校验码
        final char[] verfiyCode = {'1', '0', 'X', '9', '8', '7',
                '6', '5', '4', '3', '2'};

        // 18位身份证中，各个数字的生成校验码时的权值 加权因子
        final int[] verfiyCodeWeight = {7, 9, 10, 5, 8, 4, 2, 1,
                6, 3, 7, 9, 10, 5, 8, 4, 2};


        final char[] cs = cardNo.toUpperCase().toCharArray();
        int sum = 0, max = 17;
        for (int i = 0; i < max; i++) {
            sum += (cardNo.charAt(i) - '0') * verfiyCodeWeight[i];
        }
        int y = sum % 11;
        // 第18位校验码错误
        return verfiyCode[y] == cs[cs.length - 1];
    }

    private static String getIdcardCalendar(String certNo) {
        // 获取出生年月日
        String birthday = certNo.substring(6, 12);
        SimpleDateFormat ft = new SimpleDateFormat("yyMMdd");
        Date birthdate = null;
        try {
            birthdate = ft.parse(birthday);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        Calendar cday = Calendar.getInstance();
        if (birthdate == null) {
            return null;
        }
        cday.setTime(birthdate);
        return String.valueOf(cday.get(Calendar.YEAR));
    }


    /**
     * 0：女；1：男
     *
     * @param cardNo 身份证号码
     * @return 性别
     */
    public static int getGenderByCardNo(String cardNo) {
        boolean bb = cardNo.length() != 15 && cardNo.length() != 18;
        if (isEmpty(cardNo) || bb) {
            return -1;
        }

        String substring = (cardNo.length() == 18) ? cardNo.substring(16, 17) : cardNo.substring(13, 14);
        return Integer.parseInt(substring) % 2;
    }

    /**
     * 通过身份证获取年龄
     *
     * @param cardNo 身份证号码
     * @return 年龄
     */
    public static int getAgeByCardNo(String cardNo) {
        boolean bb = cardNo.length() != 15 && cardNo.length() != 18;
        if (isEmpty(cardNo) || bb) {
            return -1;
        }
        if (!cardNo.matches(REGEX)) {
            System.out.println("18");
            return -1;
        }

        int index = 2;
        // 校验区位码
        if (!ZONENUM.containsKey(Integer.valueOf(cardNo.substring(0, index)))) {
            return -1;
        }

        boolean isOldCard = cardNo.length() == 15;

        //校验年份
        // 7-10位是出生年份，范围应该在1900-当前年份之间
        String idcardCalendar = getIdcardCalendar(cardNo);
        if (idcardCalendar == null) {
            return -1;
        }
        int year = isOldCard ? Integer.parseInt(idcardCalendar) : Integer.parseInt(cardNo.substring(6, 10));

        return Calendar.getInstance().get(Calendar.YEAR) - year;
    }

    public static void main(String[] args) {
        System.out.println(getGenderByCardNo("412727199510124538"));
        System.out.println(checkCardNo("110110198001019716"));
        //System.out.println(checkCardNo("412727199510124538"));
        //System.out.println(checkPhone("156036287240"));
        //System.out.println(isNumber("15603628720"));
        //System.out.println(isNumber("156036287  20"));
    }
}
