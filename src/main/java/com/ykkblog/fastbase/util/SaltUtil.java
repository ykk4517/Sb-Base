package com.ykkblog.fastbase.util;

import org.springframework.util.DigestUtils;

import java.util.Arrays;

/**
 * SaltUtil
 *
 * @author YKK
 * @date 2021/8/16 18:05
 **/
public class SaltUtil {

    /**
     * 字符串Base64位加密
     *
     * @param apiKey apiKey
     * @return 加密后的apiKey
     */
    public static String base64Encode(String apiKey) {
        String encoder = Base64Util.baseEncode(apiKey);
        String start = encoder.substring(0, 1);
        String end = encoder.substring(encoder.length() - 2);
        return start + new StringBuilder(encoder.substring(1, encoder.length() - 2)).reverse().toString() + end;
    }

    /**
     * 字符串Base64位解密
     *
     * @param apiKey apiKey
     * @return String
     */
    public static String base64Decode(String apiKey) {
        String start = apiKey.substring(0, 1);
        String end = apiKey.substring(apiKey.length() - 2);
        return Base64Util.baseDecode(start + new StringBuilder(apiKey.substring(1, apiKey.length() - 2)).reverse().toString() + end);
    }


    /**
     * 字符串md5加密
     *
     * @param apiKey apiKey
     * @return 加密后的apiKey
     */
    public static String md5Encode(String apiKey) {
        String encoder = DigestUtils.md5DigestAsHex(apiKey.getBytes());
        String end = encoder.substring(encoder.length() - 1);
        int sum = 0;
        int startIndex = -1;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < encoder.length() - 1; i++) {
            char c = encoder.charAt(i);
            if (Character.isDigit(c)) {
                startIndex += 1;
                sum += Integer.parseInt(String.valueOf(c));
                stringBuilder.append(c < 57 ? (char) (c + 1) : (char) 48);
            } else {
                stringBuilder.append(c < 122 ? (char) (c + 1) : (char) 65);
            }
        }
        char[] arrayCh = stringBuilder.toString().toCharArray();
        Arrays.sort(arrayCh);
        int length = 29 - startIndex;
        int index = sum > length ? sum % length : length % sum;
        String start = String.valueOf(arrayCh[startIndex + index]).toUpperCase();
        return start + new StringBuilder(stringBuilder).reverse().toString() + end;
    }

    /**
     * 字符串md5验证
     *
     * @param apiKey apiKey
     * @return String
     */
    public static Boolean md5Valid(String apiKey, String signStr) {
        if (DataUtil.isAnyEmpty(apiKey, signStr)) {
            return false;
        }
        return signStr.equals(md5Encode(apiKey));
    }


    public static void main(String[] args) {
        String base64Encode = SaltUtil.base64Encode("test");
        System.out.println(base64Encode);
        System.out.println(SaltUtil.base64Decode(base64Encode));

        String test2 = md5Encode("scsdcsdcSDSDCSsdcSDWSDdc");
        System.out.println(test2);
        System.out.println(md5Valid("scsdcsdcSDSDCSsdcSDWSDdc", test2));

    }

}
