package com.ykkblog.fastbase.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * 文件相关工具类
 *
 * @author YKK
 * @date 2022/3/7 16:23
 **/
@Slf4j
public class FileUtil {

    /**
     * 初始化目录
     *
     * @param path 路径
     */
    public static Boolean mkDir(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            if (!file.exists()) {
                return file.mkdir();
            }
        } else if (!file.getParentFile().exists()) {
            // weather has parent dir
            return file.getParentFile().mkdir();
        }
        return false;
    }


    /**
     * save file
     *
     * @param file     file
     * @param path     path
     * @param fileName fileName
     * @return Boolean
     */
    public static Boolean save(MultipartFile file, String path, String fileName) {
        String realPath = path + File.separator + fileName;
        return save(file, realPath);
    }

    /**
     * save file
     *
     * @param file     file
     * @param realPath realPath
     * @return Boolean
     */
    public static Boolean save(MultipartFile file, String realPath) {
        File dest = new File(realPath);
        // weather has parent dir
        if (!dest.getParentFile().exists()) {
            log.info("创建目录：" + dest.getParentFile());
            if (!dest.getParentFile().mkdir()) {
                log.error("目录创建失败！");
                return false;
            }
        }
        try {
            // save file
            file.transferTo(dest);
            return true;
        } catch (IllegalStateException | IOException e) {
            log.error("文件保存失败！");
            e.printStackTrace();
            return false;
        }
    }

    public static Boolean deleteFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            log.error("文件不存在！");
            return false;
        }
        // 如果此file对象是文件的话，直接删除
        if (file.isFile()) {
            return file.delete();
        }
        return true;
    }

    /**
     * 获取文件大小 字符串
     *
     * @param file file
     * @return 文件大小 字符串
     */
    public static String getFileSize(MultipartFile file) {
        String sizeName = "b";
        double size = file.getSize();
        if (1024 <= size) {
            BigDecimal b = new BigDecimal(size);
            size = b.divide(new BigDecimal(1024), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
            sizeName = "kb";
            if (1024 <= size) {
                b = new BigDecimal(size);
                size = b.divide(new BigDecimal(1024), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
                sizeName = "mb";
            }
        }
        return size + sizeName;
    }

}
