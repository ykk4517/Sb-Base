package com.ykkblog.fastbase.util;

/**
 * 处理并记录日志文件
 *
 * @author 姚康康
 * @date 2021/9/8 23:55
 */
public class LogUtils {

    public static String getBlock(Object msg) {
        if (msg == null) {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }

}
