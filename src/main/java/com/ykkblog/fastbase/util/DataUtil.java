package com.ykkblog.fastbase.util;


import com.ykkblog.fastbase.BaseItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ykkblog.fastbase.util.IdCardUtil.REGEX;
import static java.util.regex.Pattern.compile;

/**
 * DataUtil
 *
 * @author YKK
 * @date 2021/8/17 10:47
 **/
@Slf4j
public class DataUtil {

    /**
     * 是否为空，包含空格 有空格不为空
     *
     * @param obj obj
     * @return 是否为空，包含空格 有空格不为空
     */
    public static boolean isEmpty(@Nullable Object obj) {
        return isAnyEmpty(obj);
    }

    /**
     * 是否不为空，包含空格 有空格不为空
     *
     * @param obj obj
     * @return 是否不为空，包含空格 有空格不为空
     */
    public static boolean isNotEmpty(@Nullable Object obj) {
        return !isAnyEmpty(obj);
    }

    /**
     * TODO 判断多个参数是否有任一为空
     **/
    public static Boolean isAnyEmpty(@Nullable Object... obj) {
        if (obj == null) {
            return true;
        }
        for (Object oo : obj) {
            if (oo == null || "".equals(oo)) {
                return true;
            }
            if (oo instanceof String) {
                if (((String) oo).length() == 0) {
                    return true;
                } else {
                    boolean blank = false;
                    for (int i = 0; i < ((String) oo).length(); ++i) {
                        if (!Character.isWhitespace(((String) oo).charAt(i))) {
                            blank = true;
                        }
                    }
                    if (!blank) {
                        return true;
                    }
                }
            } else if (oo instanceof Array) {
                if (oo.toString().length() == 0) {
                    return true;
                }
            } else if (oo instanceof List) {
                if (((List<?>) oo).size() == 0) {
                    return true;
                }
            } else if (oo instanceof Map) {
                return ((Map<?, ?>) oo).size() == 0;
            } else if (oo instanceof Collection) {
                return ((Collection<?>) oo).isEmpty();
            }
        }
        return false;
    }

    /**
     * 是否为空，不含空格
     *
     * @param string string
     * @return 是否为空，不含空格
     */
    public static boolean isBlank(@Nullable String string) {
        if (string == null || "".equals(string)) {
            return true;
        } else {
            for (int i = 0; i < string.length(); ++i) {
                if (!Character.isWhitespace(string.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * 是否不为空，空格也算不为空
     *
     * @param string string
     * @return 是否不为空，空格也算不为空
     */
    public static boolean isNotBlank(@Nullable String string) {
        return !isBlank(string);
    }

    /**
     * 判断多个参数是否全部为空
     *
     * @param obj obj
     * @return 是否全部为空
     */
    public static Boolean isAllEmpty(Object... obj) {
        for (Object oo : obj) {
            if (!isEmpty(oo)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 校验手机号码合法性
     *
     * @param phoneNum 手机号码
     * @return 是否合法
     */
    public static Boolean checkPhone(String phoneNum) {
        String regex = "^(1[3-9]\\d{9}$)";
        int phoneMax = 11;
        if (phoneNum.length() == phoneMax) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phoneNum);
            return m.matches();
        }
        return false;
    }

    /**
     * 是否全部是数字
     *
     * @param str 字符串
     * @return 是否全部是数字
     */
    public static Boolean isNumber(String str) {
        String regex = "^[-+]?[\\d]*$";
        Pattern pattern = compile(regex);
        return str != null && !"".equals(str) && pattern.matcher(str).matches();
    }

    /**
     * 判断对象中属性值是否全为空  全为空 返回true
     *
     * @param object object
     * @return Boolean
     */
    public static Boolean checkObjAllFieldsIsNull(Object object) {

        if (null == object || "".equals(object)) {
            return true;
        }
        try {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                System.out.print(f.getName() + ":");
                System.out.println(f.get(object));

                if (f.get(object) != null && isNotBlank(f.get(object).toString())) {
                    return false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 替换null
     *
     * @param str         str
     * @param replaceWith replaceWith
     * @return str
     */
    public static Object replaceNull(Object str, Object replaceWith) {
        return isEmpty(str) ? replaceWith : str;
    }

    /**
     * 替换空值
     *
     * @param bean bean
     * @param key  key
     * @return str
     */
    public static String replaceNull(Object bean, String key) {
        return replaceNull(bean, key, "");
    }

    /**
     * 替换空值
     *
     * @param bean bean
     * @param key  key
     * @param def  默认值
     * @return str
     */
    public static <T> T replaceNull(Object bean, String key, T def) {
        try {
            Field declaredField = bean.getClass().getDeclaredField(key);
            declaredField.setAccessible(true);
            if (DataUtil.isEmpty(declaredField.get(bean))) {
                log.warn("【" + key + "】为空！");
                return DataUtil.isEmpty(declaredField.get(bean)) ? def : (T) declaredField.get(bean);
            }
        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
            log.error("【" + key + "】不存在！");
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            log.error("【" + key + "】无法访问！");
        }
        return def;
    }

    /**
     * 拼接异常信息
     *
     * @param ex ex
     * @return 异常信息
     */
    public static String getExceptionAllinformation(Exception ex) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream pout = new PrintStream(out);
        ex.printStackTrace(pout);
        String ret = out.toString();
        try {
            pout.close();
        } catch (Exception ignored) {
        }
        try {
            out.close();
        } catch (Exception ignored) {
        }
        return ret;
    }

    /**
     * 去除字符串中的空格、回车、换行符、制表符
     * \n 回车(\u000a)
     * \t 水平制表符(\u0009)
     * \s 空格(\u0008)
     * \r 换行(\u000d)
     *
     * @param str str
     * @return String
     */
    public static String replaceBlank(String str) {

        // 或
        // return str.replaceAll(REGEX_BLANK,"");

        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile(REGEX);
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    /**
     * 获取map String值
     *
     * @param valueMap valueMap
     * @param key      key
     * @return String
     */
    public static String getMapValue(Map<String, ?> valueMap, String key) {
        return valueMap.get(key) == null ? "" : String.valueOf(valueMap.get(key));
    }

    /**
     * 获取map String值
     *
     * @param valueMap valueMap
     * @param key      key
     * @param defValue defValue
     * @return String
     */
    public static String getMapValue(Map<String, ?> valueMap, String key, String defValue) {
        if (DataUtil.isEmpty(defValue)) {
            defValue = "";
        }
        return valueMap.get(key) == null ? defValue : String.valueOf(valueMap.get(key));
    }

    /**
     * 检查指定属性是否为空
     *
     * @param bean    bean
     * @param keyList keyList
     * @return Boolean
     */
    public static Boolean paramRequiredCheck(Object bean, List<String> keyList) {
        for (String key : keyList) {
            try {
                Field declaredField = bean.getClass().getDeclaredField(key);
                declaredField.setAccessible(true);
                if (DataUtil.isEmpty(declaredField.get(bean))) {
                    log.warn("【" + key + "】为空！");
                    return false;
                }
            } catch (NoSuchFieldException e1) {
                e1.printStackTrace();
                log.error("【" + key + "】不存在！");
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                log.error("【" + key + "】无法访问！");
            }
        }
        return true;
    }

    public static void main(String[] args) {
        //System.out.println(isNumber("012 3412412467"));
        //System.out.println(isNumber("1  2 .3 412412467"));
        //System.out.println(isNumber("0  1  2 3 412412467"));
        //System.out.println(isNumber("1.?中23412412467"));
        //
        //String str = "\t\t\t4512232006012\r\r30925";
        //System.out.println(str);
        //System.out.println("111\t：" + replaceBlank(str));
        //str = "6289XXXXXX X   XXXX \n\n\n";
        //System.out.println(str);
        //System.out.println("222\t：" + replaceBlank(str));

        BaseItem baseItem = new BaseItem(1, "测试");
        baseItem.setName(null);
        String name = replaceNull(baseItem, "name", "1");
        System.out.println(name);
        Integer id = replaceNull(baseItem, "name", null);
        System.out.println(id);
    }

}
