package com.ykkblog.fastbase.util;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ykkblog.fastbase.BaseBean;
import com.ykkblog.fastbase.exception.PageException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 分页工具类
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Slf4j
public class PageUtil {

    /**
     * 分页参数有误
     *
     * @param baseBean 入参
     */
    public static void pageError(BaseBean baseBean) {
        if (baseBean.getNum() < 1 || baseBean.getSize() < 1) {
            throw new PageException("分页参数不合法！");
        }
    }

    /**
     * 设置页码和页数
     *
     * @param baseBean 入参
     */
    public static void setPage(BaseBean baseBean) {
        log.info("分页为：" + baseBean.getNum() + "\t" + baseBean.getSize());
        if (baseBean.getNum() < 1 || baseBean.getSize() < 1) {
            throw new PageException("分页参数不合法！");
        }
        PageHelper.startPage(baseBean.getNum(), baseBean.getSize());
    }

    /**
     * 获取分页对象
     *
     * @param list 数据
     * @param <T>  数据类型
     * @return 分页数据对象
     */
    public static <T> PageInfo<T> getPage(List<T> list) {
        return new PageInfo<>(list);
    }

}
