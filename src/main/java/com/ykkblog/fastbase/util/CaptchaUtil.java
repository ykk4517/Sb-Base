package com.ykkblog.fastbase.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 图片验证码工具类
 *
 * @author 姚康康
 * @date 2021/9/8 16:53
 */
public class CaptchaUtil {

    /*
     * https:// www.jb51.net/article/176380.htm
     * */

    /**
     * 图片的宽度。
     */
    private static final int WIDTH = 160;
    /**
     * 图片的高度。
     */
    private static final int HEIGHT = 40;
    /**
     * 验证码字符个数
     */
    private static final int CODE_COUNT = 4;
    /**
     * 验证码干扰线数
     **/
    private static final int LINE_COUNT = 20;

    /**
     * 验证码范围,去掉0(数字)和O(拼音)容易混淆的(小写的1和L也可以去掉,大写不用了)
     */
    private static final char[] CODE_SEQUENCE = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9'};


    // 另外一种写法 g.setColor(new Color(252, 145, 83));

    public static Map<String, String> createCode() throws IOException {

        int x, fontHeight, codeY;
        int red, green, blue;

        // 每个字符的宽度(左右各空出一个字符)
        x = WIDTH / (CODE_COUNT + 2);
        // 字体的高度
        fontHeight = HEIGHT - 2;
        codeY = HEIGHT - 4;

        // 图像buffer
        BufferedImage buffImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D gd = buffImg.createGraphics();

        // 将图像背景填充为白色
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, WIDTH, HEIGHT);
        // 增加下面代码使得背景透明

        buffImg = gd.getDeviceConfiguration().createCompatibleImage(WIDTH, HEIGHT, Transparency.TRANSLUCENT);
        gd.dispose();
        gd = buffImg.createGraphics();
        // 背景透明代码结束

        // 画图BasicStroke是JDK中提供的一个基本的画笔类,我们对他设置画笔的粗细，
        // 就可以在drawPanel上任意画出自己想要的图形了。
        gd.setColor(new Color(255, 0, 0));
        gd.setStroke(new BasicStroke(1f));
        gd.fillRect(128, 128, WIDTH, HEIGHT);

        // 生成随机数
        Random random = new Random();
        // 设置字体类型、字体大小、字体样式　
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, WIDTH, HEIGHT);

        // 创建字体，字体的大小应该根据图片的高度来定。
        Font font = new Font("微软雅黑", Font.BOLD, fontHeight);
        // 设置字体。
        gd.setFont(font);

        for (int i = 0; i < LINE_COUNT; i++) {
            // 设置随机开始和结束坐标
            // x坐标开始
            int xs = random.nextInt(WIDTH);
            // y坐标开始
            int ys = random.nextInt(HEIGHT);
            // x坐标结束
            int xe = xs + random.nextInt(WIDTH / 8);
            // y坐标结束
            int ye = ys + random.nextInt(HEIGHT / 8);

            // 产生随机的颜色值，让输出的每个干扰线的颜色值都将不同。
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            gd.setColor(new Color(red, green, blue));
            gd.drawLine(xs, ys, xe, ye);
        }

        // randomCode记录随机产生的验证码
        StringBuilder randomCode = new StringBuilder();
        // 随机产生CODE_COUNT个字符的验证码。
        for (int i = 0; i < CODE_COUNT; i++) {
            String strRand = String.valueOf(CODE_SEQUENCE[random.nextInt(CODE_SEQUENCE.length)]);
            // 产生随机的颜色值，让输出的每个字符的颜色值都将不同。
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            // 指定某种颜色
            gd.setColor(new Color(red, green, blue));
            gd.drawString(strRand, (i + 1) * x, codeY);
            // 将产生的四个随机数组合在一起。
            randomCode.append(strRand);
        }

        // 画边框。
        gd.setColor(Color.GRAY);
        gd.drawRect(0, 0, WIDTH - 1, HEIGHT - 1);


        Map<String, String> map = new HashMap<>(3);
        // 存放验证码
        map.put("captchaKey", randomCode.toString());
        // 存放生成的验证码base64字符串  BufferedImage对象转换为base64

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(buffImg, "jpg", outputStream);

        // 创建文件输出流对象
        String base64Img = Base64Util.baseEncode(outputStream.toByteArray());
        map.put("codeStr", "data:image/png;base64," + base64Img);
        return map;
    }

     /*OutputStream out = new FileOutputStream("D://img/" + System.currentTimeMillis() + ".jpg");
        ImageIO.write(buffImg, "jpg", out);*/


    public static void main(String[] args) throws Exception {
        Map<String, String> map = CaptchaUtil.createCode();
        System.out.println("验证码的值为：" + map.get("captchaKey"));
    }

}
