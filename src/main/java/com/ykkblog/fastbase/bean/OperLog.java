package com.ykkblog.fastbase.bean;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author 姚康康
 * @date 2021/9/9 20:32
 */
@Data
@Accessors(chain = true)
public class OperLog {

    /**
     * 模块标题
     */
    private String title;

    /**
     * 业务类型：0其它 1新增 2修改 3删除
     */
    private Integer businessType;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 操作类别：0其它 1后台用户 2手机端用户
     */
    private Integer operatorType;

    /**
     * 操作人员
     */
    private Integer operUserId;

    /**
     * 操作人员
     */
    private String operUsername;

    /**
     * 请求URL
     */
    private String operUrl;

    /**
     * 主机地址
     */
    private String operIp;

    /**
     * 操作地点
     */
    private String operLocation;

    /**
     * 请求参数
     */
    private String operParam;

    /**
     * 返回参数
     */
    private String jsonResult;

    /**
     * 操作状态：1正常 2异常
     */
    private Integer status;

    /**
     * 错误消息
     */
    private String errorMsg;

}