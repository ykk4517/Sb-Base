package com.ykkblog.fastbase.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * UserToken
 *
 * @author YKK
 * @date 2021/7/30 10:40
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserToken {
    /**
     * 用户id
     */
    private Integer id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 用户状态
     */
    private Integer status;
    /**
     * 令牌
     */
    private String accessToken;
    /**
     * SessionId
     */
    private String sessionId;

    /**
     * 过期时间
     */
    private Long expiredTime;

    /**
     * 最后操作时间
     */
    private String lastOperateTime;

    public UserToken(Integer id, String username, String nickname, Integer status) {
        this.id = id;
        this.username = username;
        this.nickname = nickname;
        this.status = status;
    }
}
