package com.ykkblog.fastbase;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * item Bean
 *
 * @author YKK
 * @version $Id: $Id
 */
@Data
@NoArgsConstructor
public class BaseItem {
    /**
     * 一般指id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object id;
    /**
     * 一般指编码、key或code
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object code;
    /**
     * 一般指值、value或名称
     */
    private String name;
    /**
     * 子级
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BaseItem> children;

    /**
     * 拓展信息
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> extra;

    /**
     * <p>Constructor for BaseItem.</p>
     *
     * @param id   a {@link java.lang.Integer} object.
     * @param name a {@link java.lang.String} object.
     */
    public BaseItem(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * <p>Constructor for BaseItem.</p>
     *
     * @param id   a {@link java.lang.Integer} object.
     * @param code a {@link java.lang.Integer} object.
     */
    public BaseItem(Integer id, Integer code) {
        this.id = id;
        this.code = code;
    }

    /**
     * <p>Constructor for BaseItem.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     */
    public BaseItem(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * <p>Constructor for BaseItem.</p>
     *
     * @param id   a {@link java.lang.Integer} object.
     * @param code a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     */
    public BaseItem(Object id, Object code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    /**
     * 获取Integer类型id
     *
     * @return Integer
     */
    public Integer idInteger() {
        return id == null ? null : Integer.parseInt(idString());
    }

    /**
     * 获取String类型id
     *
     * @return String
     */
    public String idString() {
        return String.valueOf(id);
    }

    /**
     * 获取Integer类型code
     *
     * @return Integer
     */
    public Integer codeInteger() {
        return code == null ? null : Integer.parseInt(codeString());
    }

    /**
     * 获取String类型code
     *
     * @return String
     */
    public String codeString() {
        return String.valueOf(code);
    }
}
