package com.ykkblog.fastbase;

import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Controller基类
 *
 * @author YKK
 * @version $Id: $Id
 */
public class BaseController {

    // protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 返回分页数据
     *
     * @param message 提示信息
     * @param page    分页对象
     * @return 返回结果
     */
    public <T> Result<List<T>> page(String message, PageInfo<T> page) {
        if (page.getList() == null || page.getList().size() == 0) {
            return error("查无数据！");
        } else {
            return ok(message, page.getList(), page.getPageNum(), page.getTotal());
        }
    }

    /**
     * <p>ok.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(String message) {
        return new Result<>(true, message);
    }

    /**
     * <p>ok.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(String message, T data) {
        return new Result<>(true, message, data);
    }

    /**
     * <p>ok.</p>
     *
     * @param status  a {@link java.lang.Integer} object.
     * @param message a {@link java.lang.String} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(Integer status, String message) {
        return new Result<>(status, message);
    }

    /**
     * <p>ok.</p>
     *
     * @param status  a {@link java.lang.Integer} object.
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(Integer status, String message, T data) {
        return new Result<>(status, message, data);
    }

    /**
     * <p>ok.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @param pageNum a {@link java.lang.Integer} object.
     * @param total   a {@link java.lang.Long} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(String message, T data, Integer pageNum, Long total) {
        return new Result<>(true, message, data, pageNum, total.intValue());
    }

    /**
     * <p>ok.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @param pageNum a {@link java.lang.Long} object.
     * @param total   a {@link java.lang.Long} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(String message, T data, Long pageNum, Long total) {
        return ok(message, data, pageNum.intValue(), total);
    }

    /**
     * <p>ok.</p>
     *
     * @param status  a {@link java.lang.Integer} object.
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @param pageNum a {@link java.lang.Integer} object.
     * @param total   a {@link java.lang.Long} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> ok(Integer status, String message, T data, Integer pageNum, Long total) {
        return new Result<>(status, message, data, pageNum, total.intValue());
    }


    /**
     * <p>error.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> error(String message) {
        return new Result<>(false, message);
    }

    /**
     * <p>error.</p>
     *
     * @param status  a {@link java.lang.Integer} object.
     * @param message a {@link java.lang.String} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> error(Integer status, String message) {
        return new Result<>(status, message);
    }

    /**
     * <p>error.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param data    a {@link java.lang.Object} object.
     * @return a {@link Result} object.
     */
    public <T> Result<T> error(String message, T data) {
        return new Result<>(false, message, data);
    }
}
